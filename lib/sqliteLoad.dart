import 'dart:convert';
import 'dart:io';
//import 'SuraResult.dart';
//import 'AyaResult.dart';
//import 'package:Halaqat/Model/AyaResult.dart';
//import 'package:Halaqat/Model/Notification.dart';
//import 'package:Halaqat/Model/StudentModel.dart';
//import 'package:Halaqat/Model/SuraResult.dart';
//import 'package:Halaqat/View/Notification/Notifacation_Page.dart';
import 'package:Halaqat/Notification.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class DBHelper {
  static Database dbInstance;

//  Future<Database> get db async {
//    var databasesPath = await getDatabasesPath();
//    var path = join(databasesPath, "copy_asset.db");
//
//    await deleteDatabase(path);
//
//    ByteData data =
//        await rootBundle.load(join("assets", 'db', "halaqat.sqlite"));
//    List<int> bytes =
//        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
//    await new File(path).writeAsBytes(bytes);
//
//    var db = await openDatabase(path, readOnly: true);
//
//    return db;
//  }
//
////  Future<Database> get db async {
////    // Construct a file path to copy database to
////    Directory documentsDirectory = await getApplicationDocumentsDirectory();
////    String path = join(documentsDirectory.path, "halaqat.sqlite");
////
////// Only copy if the database doesn't exist
////    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
////      await _loadDataBaseFormAssets(path);
////    }
////    return await openDatabase(path, version: 2);
////  }
//
//  Future<Database> get dbApp async {
//    // Construct a file path to copy database to
//    Directory documentsDirectory = await getApplicationDocumentsDirectory();
//    String path = join(documentsDirectory.path, "halaqatapp.sqlite");
//
//// Only copy if the database doesn't exist
//    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
//      await _loadDataBaseFormAssets(path);
//    }
//    return await openDatabase(path, version: 1);
//  }
//
//
//  _loadDataBaseFormAssets(path) async {
//    ByteData data =
//        await rootBundle.load(join('assets', "db", "halaqatapp.sqlite"));
//    List<int> bytes =
//        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
//    // Save copied asset to documents
//    await new File(path).writeAsBytes(bytes);
//  }
//
//  Future<List<SuraResult>> getSuraTable() async {
//    var dbConnection = await db;
//    var res = await dbConnection.query('Sura');
//
//    List<SuraResult> list =
//        res.isNotEmpty ? res.map((c) => SuraResult.fromJson(c)).toList() : [];
//    var encodedList = jsonEncode(list);
//    //return jsonDecode(encodedList);
//    return list;
//  }
//
//  Future<List<SuraResult>> getNoAya(int suraID) async {
//    var dbConnection = await db;
//    var res =
//        await dbConnection.rawQuery('SELECT * FROM Aya WHERE sura = $suraID');
//    List<int> l;
//    int i = 1;
//
//    //res.forEach((row) =>  l.add(1)); //print(row['aya']));
//
//    //List ll = List.from(res['aya'].);
//
//    //print(ll);
//
//    List<SuraResult> list =
//        res.isNotEmpty ? res.map((c) => SuraResult.fromJson(c)).toList() : [];
//    var encodedList = jsonEncode(list);
//    //return jsonDecode(encodedList);
//    return list;
//  }
//
//  Future<List<AyaResult>> getAyaTable(suraID) async {
//    var dbConnection = await db;
//    var res =
//        await dbConnection.rawQuery('SELECT * FROM Aya WHERE sura = $suraID');
//    List<AyaResult> list =
//        res.isNotEmpty ? res.map((c) => AyaResult.fromJson(c)).toList() : [];
//    var encodedList = jsonEncode(list);
//    //return jsonDecode(encodedList);
//    return list;
//  }
//
//  Future<List<AyaResult>> getAllAyaTable() async {
//    var dbConnection = await db;
//    var res = await dbConnection.rawQuery('SELECT * FROM Aya');
//    List<AyaResult> list =
//        res.isNotEmpty ? res.map((c) => AyaResult.fromJson(c)).toList() : [];
//    var encodedList = jsonEncode(list);
//    //return jsonDecode(encodedList);
//    return list;
//  }
//
//  Future<List<SuraResult>> getAllSuraTable() async {
//    var dbConnection = await db;
//    var res = await dbConnection.rawQuery('SELECT * FROM Sura');
//    List<SuraResult> list =
//        res.isNotEmpty ? res.map((c) => SuraResult.fromJson(c)).toList() : [];
//
//    return list;
//  }
//
//  Future<dynamic> getPage(pageID) async {
//    var dbConnection = await db;
//    List aya =
//        await dbConnection.rawQuery('SELECT * FROM Aya WHERE page = $pageID');
//
//    return aya.toList();
//  }
//
//  Future<dynamic> getStudentData(studentId) async {
//    var dbConnection = await db;
//    // StudentModel studentModel =new StudentModel( _id, _type, _name, _email, _phone_numbe, _password, _image, _code, _number_of_identity, _updated_at, _created_at, _parent_id) ;
//    List aya = await dbConnection
//        .rawQuery('SELECT * FROM Student WHERE id = $studentId');
//    StudentModel studentModel = new StudentModel(aya[0], aya[1], aya[2], aya[3],
//        aya[4], aya[5], aya[6], aya[7], aya[8], aya[9], aya[10], aya[11]);
//    return studentModel;
//  }
//
//  Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
//    Database db = await dbApp;
//    return await db.query(table);
//  }
//
//  Future<int> insert(Map<String, dynamic> row, String table) async {
//    Database db = await dbApp;
//
//    bool found = await isFound(row, table);
//    if (!found) {
//      return await db.insert(table, row);
//    }
//    return 0;
//  }
//
//  isFound(row, String table) async {
//    Database db = await dbApp;
//    var response =
//        await db.query(table, where: 'id = ?', whereArgs: [row["id"]]);
//
//    return response.length == 0 ? false : true;
//  }

  String notificationTable = "Notification";
  String columnNotificationValue = "value";
  String columnCreatedAt = "created_at";

  //TODO Future Notification Database
  Future <Database> get notificationDb async {
    if(dbInstance != null) {
      return dbInstance;
    }
    dbInstance = await initNotificationDb;
    return dbInstance;
  }


  Future<Database> get initNotificationDb async {

    // Construct a file path to copy database to
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "halaqatapp.sqlite");

// Only copy if the database doesn't exist
//    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
//      await _loadDataBaseFormAssets(path);
//    }
    return await openDatabase(path, version: 1, onCreate: _onCreateNotification);
  }

  void _onCreateNotification (Database db, int version) async {
    db.execute(
        "create table $notificationTable (id INTEGER PRIMARY KEY, $columnNotificationValue TEXT, $columnCreatedAt TEXT, type INTEGER)"
    );
    
    print("Notification table is created Successefuly!");
  }

  //TODO Save Notifications
  Future<int> saveNotifications(Notifications notification) async {
    var dbClient = await notificationDb;

    var res = await dbClient.insert(notificationTable, notification.toMap());

    return res;
  }

  //TODO Get All Notifications
  Future<List> getAllNotifications (int type) async {
    var dbClient = await notificationDb;

    var res = await dbClient.rawQuery(
      "Select * from $notificationTable where type = $type"
    );

    return res.toList();
  }

  //TODO Get One Notification
  Future<Notifications> getNotification (int id, int type) async {
    var dbClient = await notificationDb;
    var res = await dbClient.rawQuery("select * from $notificationTable where id = $id AND type = $type");
    if(res.length == 0) return null;

    return Notifications.fromMap(res.first);
  }

  //TODO Delete Notification
  Future<int> deleteNotification(int id) async {
    var dbClient = await notificationDb;

    return await dbClient.delete("$notificationTable", where: "$id = ?" , whereArgs: [id]);
  }

  //TODO Update Notification
  Future<int> updateNotification(Notifications notification) async {
    var dbClient = await notificationDb;

    return await dbClient.update("$notificationTable", notification.toMap(),
      where: "id = ?",
      whereArgs: [notification.id]
    );
  }

  //TODO Close
  Future close() async {
    var dbClient = await notificationDb;

    return await dbClient.close();
  }
}
