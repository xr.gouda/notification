//import 'package:Halaqat/Controller/sqliteLoad.dart';
//import 'package:Halaqat/Model/Notification.dart';
//import 'package:Halaqat/View/Drawer/DrawerWidget.dart';
//import 'package:Halaqat/View/SliverAppBar.dart';
import 'package:Halaqat/Notification.dart';
import 'package:Halaqat/sqliteLoad.dart';
import 'package:flutter/material.dart';
import 'package:hijri/umm_alqura_calendar.dart';

int type;

var dateHigri;
var dateHigriView;

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    type = 0;
    var ummAlquracalendar = new ummAlquraCalendar.now();
    ummAlquracalendar.currentLocale = 'ar';

    dateHigriView = "${ummAlquracalendar.hDay}/${ummAlquracalendar.hMonth}/${ummAlquracalendar.hYear}";
    readNotification();
  }

  TextEditingController _notificationController = TextEditingController();

  var db = DBHelper();

  bool isGeneral = true;
  bool isPositive = false;
  bool isNegative = false;

  final List<Notifications> _notificationList = <Notifications>[];

  void _saveNotification(String text, type) async {
    _notificationController.clear();

    var createdAt = new ummAlquraCalendar.now();
    createdAt.currentLocale = 'ar';

    dateHigri = "${createdAt.hDay}/${createdAt.hMonth}/${createdAt.hYear}";

    try {
      Notifications notification = Notifications(type, text, dateHigri);

      int savedItemId = await db.saveNotifications(notification);

      Notifications addedNotification =
          await db.getNotification(savedItemId, type);

      setState(() {
        _notificationList.add( addedNotification);
      });

      print(
          "Notification saved Successefuly && Id => $savedItemId, type => $type");
    } catch (e) {
      print("ERORR is $e");
    }
  }

  readNotification() async {
    List notification = await db.getAllNotifications(type);

    notification.forEach((notifications) {
      Notifications notification = Notifications.fromMap(notifications);

      setState(() {
        _notificationList.add(Notifications.map(notifications));
      });

      print("Notification title is => ${notification.value}");
    });
  }

  void notificationDialog() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(23),
                      border: Border.all(
                        width: 1,
                        color: Colors.teal[700],
                      ),
                    ),
                    child: TextField(
                      controller: _notificationController,
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                          hintText: 'ملاحظة',
                          contentPadding: EdgeInsets.only(bottom: 1, top: 5),
                          border: InputBorder.none),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      RaisedButton(
                        child: Text(
                          "حفظ",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          _saveNotification(_notificationController.text, type);
                          _notificationController.clear();
                          Navigator.pop(context);
                        },
                        color: Colors.teal[700],
                      ),
                      RaisedButton(
                        child: Text(
                          "إلغاء",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () => Navigator.pop(context),
                        color: Colors.teal[700],
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  void updateNotificationDialog(Notifications notification, int index) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(23),
                      border: Border.all(
                        width: 1,
                        color: Colors.teal[700],
                      ),
                    ),
                    child: TextField(
                      controller: _notificationController,
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                          hintText: 'ملاحظة',
                          contentPadding: EdgeInsets.only(bottom: 1, top: 5),
                          border: InputBorder.none),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      RaisedButton(
                        child: Text(
                          "حفظ",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () async {
                          Notifications newNotification =
                              Notifications.fromMap({
                            "id": notification.id,
                            "type": type,
                            "value": _notificationController.text,
                            "created_at": dateHigriView
                          });
                          _updateNotification(notification, index);
                          _notificationController.clear();
                          await db.updateNotification(newNotification);
                          setState(() {
                            _notificationList.clear();
                            readNotification();
                          });
                          Navigator.pop(context);
                        },
                        color: Colors.teal[700],
                      ),
                      RaisedButton(
                        child: Text(
                          "إلغاء",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () => Navigator.pop(context),
                        color: Colors.teal[700],
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  void _updateNotification(Notifications notification, int index) {
    setState(() {
     _notificationList[index].value = notification.value;
    });
  }

  deleteNotification(int id, int index) async {
    print("Item Deleted Successfuly!");

    await db.deleteNotification(id);

    setState(() {
      _notificationList.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      endDrawer: halaqatDrawer(context),
      body: CustomScrollView(
        slivers: <Widget>[
//          sliverAppBar(
//              context: context,
//              icon: Icons.event_note,
//              iconName: "notification",
//              iconNameSize: 11),
          SliverList(
            delegate: SliverChildListDelegate([
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                color: Colors.teal[700],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        setState(() {
                          isNegative = !isNegative;
                          isGeneral = false;
                          isPositive = false;
                          type = 2;
                          _notificationList.clear();
                          readNotification();
                        });
                        print("سلبي");
                      },
                      child: Container(
                        child: Text("إشعار سلبي",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          isPositive = !isPositive;
                          isGeneral = false;
                          isNegative = false;
                          type = 1;
                          _notificationList.clear();
                          readNotification();
                        });
                        print("ايجابي");
                      },
                      child: Container(
                        child: Text("إشعار إيجابي",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          isGeneral = !isGeneral;
                          isPositive = false;
                          isNegative = false;
                          type = 0;
                          _notificationList.clear();
                          readNotification();
                        });
                        print("عام");
                      },
                      child: Container(
                        child: Text("إشعار عام",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ],
                ),
              )
            ]),
          ),
          types()
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.teal[700],
        onPressed: notificationDialog,
      ),
    );
  }

  Widget types() {
    if (isPositive == true &&
        isGeneral == false &&
        isNegative == false &&
        type == 1) {
      isPositive = false;
      return SliverList(
          delegate: SliverChildBuilderDelegate((_, int index) {
        return Column(
          children: <Widget>[
            //Notification Title
            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(right: 8),
              height: 30,
              width: MediaQuery.of(context).size.width - 20,
              decoration: BoxDecoration(
                color: Colors.tealAccent[700],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                _notificationList[index].value,
                style: TextStyle(color: Colors.white),
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    SizedBox(width: 10),
                    InkWell(
                      onTap: () => deleteNotification(
                          _notificationList[index].id, index),
                      child: Icon(
                        Icons.delete,
                        size: 28,
                        color: Colors.teal,
                      ),
                    ),
                    SizedBox(width: 15),
                    InkWell(
                        onTap: () => updateNotificationDialog(
                            _notificationList[index], index),
                        child: Icon(Icons.edit, color: Colors.teal, size: 28)),
                  ],
                ),

                //Date Text
                Row(
                  children: <Widget>[
                    Text(_notificationList[index].createdAt.toString()),
                    SizedBox(width: 12)
                  ],
                )
              ],
            ),
          ],
        );
      }, childCount: _notificationList.length));
    } else if (isNegative == true &&
        isGeneral == false &&
        isPositive == false &&
        type == 2) {
      isNegative = false;
      return SliverList(
          delegate: SliverChildBuilderDelegate((_, int index) {
        return Column(
          children: <Widget>[
            //Notification Title
            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(right: 8),
              height: 30,
              width: MediaQuery.of(context).size.width - 20,
              decoration: BoxDecoration(
                color: Colors.tealAccent[700],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                _notificationList[index].value,
                style: TextStyle(color: Colors.white),
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    SizedBox(width: 10),
                    InkWell(
                      onTap: () => deleteNotification(
                          _notificationList[index].id, index),
                      child: Icon(
                        Icons.delete,
                        size: 28,
                        color: Colors.teal,
                      ),
                    ),
                    SizedBox(width: 15),
                    InkWell(
                        onTap: () => updateNotificationDialog(
                            _notificationList[index], index),
                        child: Icon(Icons.edit, color: Colors.teal, size: 28)),
                  ],
                ),

                //Date Text
                Row(
                  children: <Widget>[
                    Text(_notificationList[index].createdAt.toString()),
                    SizedBox(width: 12)
                  ],
                )
              ],
            ),
          ],
        );
      }, childCount: _notificationList.length));
    }

    return SliverList(
        delegate: SliverChildBuilderDelegate((_, int index) {
      return Column(
        children: <Widget>[
          //Notification Title
          Container(
            alignment: Alignment.centerRight,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.only(right: 8),
            height: 30,
            width: MediaQuery.of(context).size.width - 20,
            decoration: BoxDecoration(
              color: Colors.tealAccent[700],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Text(
              _notificationList[index].value,
              style: TextStyle(color: Colors.white),
            ),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  SizedBox(width: 10),
                  InkWell(
                    onTap: () =>
                        deleteNotification(_notificationList[index].id, index),
                    child: Icon(
                      Icons.delete,
                      size: 28,
                      color: Colors.teal,
                    ),
                  ),
                  SizedBox(width: 15),
                  InkWell(
                      onTap: () => updateNotificationDialog(
                          _notificationList[index], index),
                      child: Icon(Icons.edit, color: Colors.teal, size: 28)),
                ],
              ),

              //Date Text
              Row(
                children: <Widget>[
                  Text(_notificationList[index].createdAt.toString()),
                  SizedBox(width: 12)
                ],
              )
            ],
          ),
        ],
      );
    }, childCount: _notificationList.length));
  }
}
