
class Notifications {

  var _id;
  int _type;
  String _value;
//  int _studentId;
//  int _teacherId;
//  int _halaqaId;
  String _createdAt;
//  int _updatedAt;


  Notifications(
//      this._id,
      this._type,
      this._value,
//      this._studentId,
//      this._teacherId,
//      this._halaqaId,
      this._createdAt,
//      this._updatedAt
      );

  set value(String value) {
    _value = value;
  }

  Notifications.map (Map<String, dynamic> obj) {
    this._id = obj ['id'];
    this._type = obj['type'];
    this._value = obj ['value'];
//    this._studentId = obj ['studentId'];
//    this._teacherId = obj ['teacherId'];
//    this._halaqaId = obj ['halaqaId'];
    this._createdAt = obj ['created_at'];
//    this._updatedAt = obj ['updatedAt'];
  }

  int get id => _id;
  int get type => _type;
  String get value => _value;
//  int get studentId => _studentId;
//  int get teacherId => _teacherId;
//  int get halaqaId => _halaqaId;
  String get createdAt => _createdAt;
//  int get updatedAt => _updatedAt;


  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['id'] = _id;
    map['type'] = _type;
    map['value'] = _value;
//    map['studentId'] = _studentId;
//    map['teacherId'] = _teacherId;
//    map['halaqaId'] = _halaqaId;
    map['created_at'] = _createdAt;
//    map['updatedAt'] = _updatedAt;

    return map;
  }


  Notifications.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._type = map['type'];
    this._value = map['value'];
//    this._studentId = map['studentId'];
//    this._teacherId = map['teacherId'];
//    this._halaqaId = map['halaqaId'];
    this._createdAt = map['created_at'];
//    this._updatedAt = map['updatedAt'];
  }

}
